# Adapted from https://github.com/GoogleCloudPlatform/vertex-ai-samples

FROM pytorch/torchserve:latest-cpu

USER model-server

# copy model artifacts
COPY ./artifacts /home/model-server/artifacts
COPY ./build_model_archive.sh /home/model-server/

# create model archives from artifacts
RUN mkdir /home/model-server/models
RUN /home/model-server/build_model_archive.sh

# create torchserve configuration file
USER root
RUN printf "\ninference_address=http://0.0.0.0:7080" >> /home/model-server/config.properties
RUN printf "\nmanagement_address=http://0.0.0.0:7081" >> /home/model-server/config.properties
USER model-server

# expose health and prediction listener ports from the image
EXPOSE 7080
EXPOSE 7081

# run Torchserve HTTP serve to respond to prediction requests
CMD ["torchserve", \
     "--start", \
     "--ts-config=/home/model-server/config.properties", \
     "--models", \
     "all", \
     "--model-store", \
     "/home/model-server/models"]