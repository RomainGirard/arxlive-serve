# arxlive-serve - Serving axlive models into prodution

## Setup

Use `poetry` to setup the environment and install the required dependencies:

```sh
poetry install
```

Activate the virtual environment in your terminal before running any python-related command

```sh
poetry shell
```

Set up a model store directory

```sh
mkdir model_store
```


## Usage

Save an archived model (created using `torch-model-archiver`) into the `model_store` directory - here, a fictitious `baseline.mar` model will be used as an example.

Serve the model using `torchserve`

```sh
torchserve --start --model-store model_store --models baseline=baseline.mar
```

See the torchserve documentation on how to send requests to the model