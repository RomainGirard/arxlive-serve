#!/bin/sh

search_dir=artifacts/

for artifact in "$search_dir"/*.tar.gz
do
    echo " Building $artifact archive"
    mkdir -p _tmp && tar -xvf "$artifact" -C _tmp
    cd _tmp || exit

    export_path=../models
    model_name=$(echo "$artifact"|cut -d "_" -f1|cut -d "/" -f3)

    rm -f "$export_path/$model_name.mar" || return
    torch-model-archiver --model-name "$model_name" --version 0.1 --model-file model.py --serialized-file model_best_state_dict.pth --handler text_classifier --extra-file "index_to_name.json,source_vocab.pt" --export-path "$export_path"

    cd ..
    rm -r _tmp
done